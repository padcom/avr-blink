//-------------------------------------------------------------------------------------------------
// Wyświetlacz alfanumeryczny ze sterownikiem HD44780
// Sterowanie w trybie 8-bitowym bez odczytu flagi zajętości
// Plik : HD44780.c
// Mikrokontroler : Atmel AVR
// Kompilator : avr-gcc
// Autor : Radosław Kwiecień
// Źródło : http://radzio.dxp.pl/hd44780/
// Data : 24.03.2007
//-------------------------------------------------------------------------------------------------

#include <stdlib.h>

#include "config.h"
#include "hd44780.h"

//-------------------------------------------------------------------------------------------------
//
// Funkcja wystawiająca półbajt na magistralę danych
//
//-------------------------------------------------------------------------------------------------
void _lcd_out_nibble(unsigned char nibbleToWrite) {
	if (nibbleToWrite & 0x01)
		LCD_DB4_PORT |= LCD_DB4;
	else
		LCD_DB4_PORT &= ~LCD_DB4;

	if (nibbleToWrite & 0x02)
		LCD_DB5_PORT |= LCD_DB5;
	else
		LCD_DB5_PORT &= ~LCD_DB5;

	if (nibbleToWrite & 0x04)
		LCD_DB6_PORT |= LCD_DB6;
	else
		LCD_DB6_PORT &= ~LCD_DB6;

	if (nibbleToWrite & 0x08)
		LCD_DB7_PORT |= LCD_DB7;
	else
		LCD_DB7_PORT &= ~LCD_DB7;
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu bajtu do wyświetacza (bez rozróżnienia instrukcja/dane).
//
//-------------------------------------------------------------------------------------------------
void _lcd_write(unsigned char data) {
	LCD_E_PORT |= LCD_E;
	_lcd_out_nibble(data >> 4);
	LCD_E_PORT &= ~LCD_E;
	LCD_E_PORT |= LCD_E;
	_lcd_out_nibble(data);
	LCD_E_PORT &= ~LCD_E;
	_delay_us(50);
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu rozkazu do wyświetlacza
//
//-------------------------------------------------------------------------------------------------
void lcd_command(unsigned char command) {
	LCD_RS_PORT &= ~LCD_RS;
	_lcd_write(command);
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu danych do pamięci wyświetlacza
//
//-------------------------------------------------------------------------------------------------
void lcd_data(unsigned char dataToWrite) {
	LCD_RS_PORT |= LCD_RS;
	_lcd_write(dataToWrite);
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja wyświetlenia napisu na wyświetlaczu.
//
//-------------------------------------------------------------------------------------------------
void lcd_text(char * text) {
	while (*text) lcd_data(*text++);
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja wyświetlenia liczby zmiennoprzecinkowej na wyświetlaczu.
//
//-------------------------------------------------------------------------------------------------
void lcd_double(double data) {
	char buff[10];
	dtostrf(data, 3, 1, buff);
	lcd_text(buff);
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja ustawienia współrzędnych ekranowych
//
//-------------------------------------------------------------------------------------------------
void lcd_goto(unsigned char x, unsigned char y) {
	lcd_command(HD44780_DDRAM_SET | (x + (0x40 * y)));
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja czyszczenia ekranu wyświetlacza.
//
//-------------------------------------------------------------------------------------------------
void lcd_clear(void) {
	lcd_command(HD44780_CLEAR);
	_delay_ms(2);
}

//-------------------------------------------------------------------------------------------------
//
// Funkcja przywrócenia początkowych współrzędnych wyświetlacza.
//
//-------------------------------------------------------------------------------------------------
void lcd_home(void) {
	lcd_command(HD44780_HOME);
	_delay_ms(2);
}

//-------------------------------------------------------------------------------------------------
//
// Procedura inicjalizacji kontrolera HD44780.
//
//-------------------------------------------------------------------------------------------------
void lcd_init(void) {
	int i;
	LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadzeń
	LCD_DB5_DIR |= LCD_DB5; //
	LCD_DB6_DIR |= LCD_DB6; //
	LCD_DB7_DIR |= LCD_DB7; //
	LCD_E_DIR 	|= LCD_E;   //
	LCD_RS_DIR 	|= LCD_RS;  //
	_delay_ms(15); // oczekiwanie na ustalibizowanie się napiecia zasilajacego
	LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
	LCD_E_PORT  &= ~LCD_E;  // wyzerowanie linii E

	// trzykrotne powtórzenie bloku instrukcji
	for (i = 0; i < 3; i++) {
		LCD_E_PORT |= LCD_E; //  E = 1
		_lcd_out_nibble(0x03); // tryb 8-bitowy
		LCD_E_PORT &= ~LCD_E; // E = 0
		_delay_ms(5); // czekaj 5ms
	}

	LCD_E_PORT |= LCD_E; // E = 1
	_lcd_out_nibble(0x02); // tryb 4-bitowy
	LCD_E_PORT &= ~LCD_E; // E = 0

	_delay_ms(1); // czekaj 1ms
	lcd_command(HD44780_FUNCTION_SET | HD44780_FONT5x8 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
	lcd_command(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wyłączenie wyswietlacza
	lcd_command(HD44780_CLEAR); // czyszczenie zawartości pamieci DDRAM
	_delay_ms(2);
	lcd_command(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
	lcd_command(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // włącz LCD, bez kursora i mrugania
}

//-------------------------------------------------------------------------------------------------
//
// Koniec pliku HD44780.c
//
//-------------------------------------------------------------------------------------------------
