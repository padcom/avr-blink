#ifndef _USART_H
#define _USART_H

#include <stdio.h>

extern FILE usart_output;
extern FILE usart_input;

void usart_init();
void usart_putchar(char c);
char usart_getchar();

#endif /* _USART_H */
