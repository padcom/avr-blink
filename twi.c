#include <util/delay.h>
#include <util/twi.h>

#include "twi.h"

void twi_init(uint8_t bitrate, uint8_t prescaler) {
	TWBR = bitrate;
	// mask off the high prescaler bits because we only need the lower three bits
	TWSR = prescaler & 0x03;
}

uint8_t twi_action(uint8_t command) {
	// Write command to TWCR and make sure TWINT is set
	TWCR = command | (1<<TWINT) | (1<<TWEN);
	// Now wait for TWINT to be set again (when the operation is completed)
	while (!(TWCR & (1<<TWINT)));
	// Return the result
	return TWSR & 0xF8;
}

uint8_t twi_start() {
	return twi_action(1<<TWSTA);
}

void twi_stop() {
	// Both byte write and page write are terminated with a stop condition
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
	// Wait a bit for the stop to take effect
	_delay_us(100);
}

uint8_t twi_read(uint8_t put_ack, uint8_t *data) {
	uint8_t result;
	if (put_ack) {
		result = twi_action(1<<TWEA);
	} else {
		result = twi_action(0);
	}
	*data = TWDR;
	return result;
}

uint8_t twi_write(uint8_t data) {
	TWDR = data;
	return twi_action(0);
}

void twi_wait(uint8_t slave) {
	do {
		twi_start();
	} while(twi_write(slave) != TW_MT_SLA_ACK);
}
