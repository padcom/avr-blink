# AVR Makefile 1.0 - Matthias Hryniszak

# ------------------------------------------------------------------------------
# General settings
# ------------------------------------------------------------------------------

PROGRAM=blink
MCU=atmega328p
FREQUENCY=16000000

# avrdude settings
PROGRAMER=arduino
PROGRAMER_PORT=/dev/ttyACM0
PROGRAMER_FLAGS=-b 57600

# project settings
DEPS=config.h util.h
OBJECTS=main.o led.o usart.o twi.o 24lc256.o hd44780.o dht.o enc28j60.o ip_arp_udp_tcp.o websrv_help_functions.o

# ------------------------------------------------------------------------------
# Compilation definition
# ------------------------------------------------------------------------------

CC=avr-gcc
CFLAGS=-c -g -Os -s -mmcu=$(MCU) -fdata-sections -ffunction-sections -I. -DF_CPU=$(FREQUENCY)

# ------------------------------------------------------------------------------
# Compilation rules
# ------------------------------------------------------------------------------

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(PROGRAM).elf: $(OBJECTS)
	$(CC) -Os -s -Wl,--gc-sections -mmcu=$(MCU) -o $(PROGRAM).elf $(OBJECTS) -I. -lm
	avr-size -C --mcu=$(MCU) -x $(PROGRAM).elf

all: $(PROGRAM).elf

%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $< $@

load: $(PROGRAM).hex
	avrdude -p $(MCU) -c $(PROGRAMER) -P $(PROGRAMER_PORT) $(PROGRAMER_FLAGS) -D -U flash:w:$(PROGRAM).hex:i

clean:
	rm -f $(OBJECTS) $(PROGRAM).elf $(PROGRAM).hex
