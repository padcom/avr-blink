#ifndef _24LC256_H
#define _24LC256_H

char ee_24lc256_read(uint8_t slave, uint16_t address, char *data, uint8_t size);
char ee_24lc256_write(uint8_t slave, uint16_t address, char *data, uint8_t size);

#endif /* _24LC256_H */
