#ifndef _LED_H
#define _LED_H

void led_init(volatile uint8_t * config_port, volatile uint8_t * port, uint8_t bit);
void led_on();
void led_off();
void led_toggle();

#endif /* _LED_H */
