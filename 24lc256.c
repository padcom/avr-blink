#include <stdio.h>
#include <util/twi.h>
#include "twi.h"

volatile char ee_24lc256_write(uint8_t slave, uint16_t address, char *data, uint8_t size) {
	TWCR = 0;
	uint8_t status = twi_start();
	int i;
	if (status != TW_START && status != TW_REP_START) return TWSR;
	if (twi_write(slave) != TW_MT_SLA_ACK) return TWSR;
	if (twi_write(address >> 8) != TW_MT_DATA_ACK) return TWSR;
	if (twi_write(address & 255) != TW_MT_DATA_ACK) return TWSR;
	for (i = 0; i < size; i++) {
		if (twi_write(*data) != TW_MT_DATA_ACK) return TWSR;
		data++;
	}
	twi_stop();
	twi_wait(slave);
	return 0;
}

volatile char ee_24lc256_read(uint8_t slave, uint16_t address, char *data, uint8_t size) {
	TWCR = 0;
	uint8_t status = twi_start();
	int i;
	if (status != TW_START && status != TW_REP_START) return TWSR;
	if (twi_write(slave) != TW_MT_SLA_ACK) return TWSR;
	if (twi_write(address >> 8) != TW_MT_DATA_ACK) return TWSR;
	if (twi_write(address & 255) != TW_MT_DATA_ACK) return TWSR;
	if (twi_start() != TW_REP_START) return TWSR;
	if (twi_write(slave | 0x01) != TW_MR_SLA_ACK) return TWSR;
	for (i = 0; i < size; i++) {
		if (twi_read(1, data) != TW_MR_DATA_ACK) return TWSR;
		data++;
	}
	twi_stop();
	return 0;
}
