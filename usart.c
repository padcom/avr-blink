#include <stdio.h>
#include <avr/io.h>

#include "config.h"
#include "usart.h"

#include <util/setbaud.h>

void usart_putchar2(char c, FILE *stream) {
	if (c == '\n') {
		usart_putchar2('\r', stream);
	}
	usart_putchar(c);
}

char usart_getchar2(FILE *stream) {
	return usart_getchar();
}

FILE usart_output = FDEV_SETUP_STREAM(usart_putchar2, NULL, _FDEV_SETUP_WRITE);
FILE usart_input = FDEV_SETUP_STREAM(NULL, usart_getchar2, _FDEV_SETUP_READ);

void usart_init() {
	// initialize serial port
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;

#if USE_2X
	UCSR0A |= _BV(U2X0);
#else
	UCSR0A &= ~(_BV(U2X0));
#endif

	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
	UCSR0B = _BV(RXEN0) | _BV(TXEN0); /* Enable RX and TX */

	stdout = &usart_output;
	stdin = &usart_input;
}

void usart_putchar(char c) {
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

char usart_getchar() {
	loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
	return UDR0;
}
