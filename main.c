#include <avr/io.h>
#include <util/delay.h>

#include "config.h"
#include "util.h"
#include "led.h"
#include "usart.h"
#include "enc28j60.h"
#include "twi.h"

static uint8_t mac_address[6] = { 0x22, 0x22, 0x22, 0x10, 0x00, 0x33};
static uint8_t local_host[4]  = { 192, 168, 32, 191};
static uint8_t local_mask[4]  = { 255, 255, 255, 0};
static uint8_t ext_address[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
static uint8_t remote_host[4] = { 192, 168, 32, 21};
static uint8_t gwip[4]        = { 192, 168, 32, 1};
static uint16_t local_port    = 5000;
static uint16_t remote_port   = 5000;

static uint8_t buf[750];

static char data[] = "Hello, world!\n";

int counter = 0;
int _gw_arp_state = 0;

#define GW_ARP_REQUEST  0
#define GW_ARP_RESPONSE 1
#define GW_ARP_IDLE     2

static uint8_t gwmac[6]       = { 0, 0, 0, 0, 0, 0 };

void net_loop(int data_p) {

}

int main() {
	twi_init(twi_frequency(500L), 0);
	led_init(&LED_CONFIG_PORT, &LED_PORT, LED_PIN);
	led_off();
	usart_init();
	puts("Hello, world!\n");

	// initialize ethernet card
	_delay_loop_1(0);
	enc28j60Init(mac_address);
	enc28j60clkout(2);
	_delay_loop_1(0);
	enc28j60PhyWrite(PHLCON, 0x476);
	_delay_loop_1(0);
	init_mac(mac_address);
	client_ifconfig(local_host, local_mask);

	while (1) {
		int data_p = packetloop_arp_icmp_tcp(buf, enc28j60PacketReceive(sizeof(buf) - 1, buf));

		if (counter++ % 20 == 0) {
			send_udp(buf, data, sizeof(data), local_port, remote_host, remote_port, ext_address);
		}
		led_toggle();
		_delay_ms(100);
	}
}
