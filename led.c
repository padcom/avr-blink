#include <avr/io.h>

#include "util.h"
#include "led.h"

volatile uint8_t * led_port;
uint8_t led_bit;

void led_init(volatile uint8_t * config_port, volatile uint8_t * port, uint8_t bit) {
	led_port = port;
	led_bit = bit;
	sbi(*config_port, led_bit);
}

void led_on() {
	sbi(*led_port, led_bit);
}

void led_off() {
	cbi(*led_port, led_bit);
}

void led_toggle() {
	tbi(*led_port, led_bit);
}
