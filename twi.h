#ifndef _TWI_H
#define _TWI_H

#define twi_frequency(frequency)  (uint8_t) (((F_CPU/frequency)-16)/2)

void twi_init(uint8_t bitrate, uint8_t prescaler);
uint8_t twi_action(uint8_t command);
uint8_t twi_start();
void twi_stop();
uint8_t twi_read(uint8_t put_ack, uint8_t *data);
uint8_t twi_write(uint8_t data);
void twi_wait(uint8_t slave);

#endif /* _TWI_H */
