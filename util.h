#ifndef _UTIL_H
#define _UTIL_H

#define sbi(port, bit) ((port) |= (1 << (bit)))
#define cbi(port, bit) ((port) &= ~(1 << (bit)))
#define tbi(port, bit) ((port) ^= (1 << (bit)))

#endif /* _UTIL_H */
