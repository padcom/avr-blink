/*
DHT Library 0x03

copyright (c) Davide Gironi, 2012

Released under GPLv3.
Please refer to LICENSE file for licensing information.

References:
  - DHT-11 Library, by Charalampos Andrianakis on 18/12/11
*/

#ifndef _DHT_H
#define _DHT_H

#include <stdio.h>
#include <avr/io.h>

// enable decimal precision (float)
#if DHT_TYPE == DHT_DHT11
  #define DHT_FLOAT 0
#elif DHT_TYPE == DHT_DHT22
  #define DHT_FLOAT 1
#endif

//timeout retries
#define DHT_TIMEOUT  200

//functions
void dht_init();
#if DHT_FLOAT == 1
  extern int8_t dht_gettemperature(double *temperature);
  extern int8_t dht_gethumidity(double *humidity);
  extern int8_t dht_gettemperaturehumidity(double *temperature, double *humidity);
#elif DHT_FLOAT == 0
  extern int8_t dht_gettemperature(int8_t *temperature);
  extern int8_t dht_gethumidity(int8_t *humidity);
  extern int8_t dht_gettemperaturehumidity(int8_t *temperature, int8_t *humidity);
#endif

#endif /* _DHT_H */
