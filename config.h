#ifndef _CONFIG_H
#define _CONFIG_H

// CPU
#define F_CPU 16000000

// Serial port
#define BAUD 9600

// LED
#define LED_CONFIG_PORT DDRB
#define LED_PORT        PORTB
#define LED_PIN         PB0

// LCD
#define LCD_RS_DIR      DDRC
#define LCD_RS_PORT     PORTC
#define LCD_RS          (1 << PC3)
#define LCD_E_DIR       DDRC
#define LCD_E_PORT      PORTC
#define LCD_E           (1 << PC2)
#define LCD_DB4_DIR     DDRB
#define LCD_DB4_PORT    PORTB
#define LCD_DB4         (1 << PB5)
#define LCD_DB5_DIR     DDRB
#define LCD_DB5_PORT    PORTB
#define LCD_DB5         (1 << PB4)
#define LCD_DB6_DIR     DDRB
#define LCD_DB6_PORT    PORTB
#define LCD_DB6         (1 << PB3)
#define LCD_DB7_DIR     DDRB
#define LCD_DB7_PORT    PORTB
#define LCD_DB7         (1 << PB2)

// DHT - port and sensor type
#define DHT_DDR         DDRB
#define DHT_PORT        PORTB
#define DHT_PIN         PINB
#define DHT_INPUTPIN    PB0
#define DHT_DHT11       1
#define DHT_DHT22       2
#define DHT_TYPE        DHT_DHT22

#endif /* _CONFIG_H */
